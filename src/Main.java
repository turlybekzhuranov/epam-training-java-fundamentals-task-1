public class Main {
    public static void main(String[] args){
        greetUser(getRequestedLine("What is your name?"));
        System.out.println(getSumAndMultiplication());
    }
    static String getRequestedLine(String requestMessage){
        Requester requester = new Requester();
        return requester.requestLine(requestMessage);
    }
    static void greetUser(String name){
        System.out.println("Hello, " + name);
    }
    static String getSumAndMultiplication(){
        String[] numbers = getRequestedLine("Enter numbers (separated by space):").trim().split("\\s+");
        int sum = 0, multiplication = 1;
        for (String number: numbers){
            if (isNumeric(number)){
                int validNUmber = Integer.parseInt(number);
                sum += validNUmber;
                multiplication *= validNUmber;
            }
            else {
                System.out.println("Your enter has non-numeric value");
                return getSumAndMultiplication();
            }
        }
        return "Sum of numbers is " + sum + "\nMultiplication of numbers is " + multiplication;
    }
    static boolean isNumeric(String string){
        return string.matches("-?\\d+(\\.\\d+)?");
    }
}