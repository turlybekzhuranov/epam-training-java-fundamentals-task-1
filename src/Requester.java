import java.util.Scanner;

public class Requester{
    public final Scanner scanner;

    public Requester(){
        this.scanner = new Scanner(System.in);
    }

    public String requestLine(String requestMessage){
        System.out.println(requestMessage);
        return scanner.nextLine();
    }
}
